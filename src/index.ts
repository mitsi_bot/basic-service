import express, { Response, Request } from "express";
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import BaseService from 'mitsi_bot_lib/dist/src/BaseService';

class Server extends BaseService {
  onPost(request: Request, response: Response): void {    
    response.json(new ResponseText(`Basic service, should be requested on TIME intent.`));
  }
}

new Server(express()).startServer();